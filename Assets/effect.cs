﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class effect : MonoBehaviour
{
    public Grid grid;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseDown()
    {
        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3Int coordinate = grid.WorldToCell(mouseWorldPos);
        Tilemap TM = gameObject.GetComponent(typeof(Tilemap)) as Tilemap;
        Tile TB = TM.GetTile(coordinate) as Tile;
        // If your mouse hovers over the GameObject with the script attached, output this message
        // ref  color = Color.red;
        Debug.Log("Scripted");
    }

}
