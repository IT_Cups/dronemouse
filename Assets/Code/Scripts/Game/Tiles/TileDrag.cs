﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TileDrag : MonoBehaviour
{
	public enum MovementType
	{
		Linear
	}


	public Direction currentDirection = Direction.None;

	public Axis MovementAxis { get => movementAxis;}
	public GridControl GridControl { get => gridControl;}
	public Vector3 StartingTilePosition { get => startingTilePosition;}
	public Vector3 LastRecievedPosition { get => lastRecievedPosition.Value; }
	public Tile_Draggable SelectedTile { get => selectedTile;}
	public bool IsMoving { get; private set; } = false;

	public Vector3 relativePos;
	public float distance;
	private Axis movementAxis;
	private Axis previousAxis;
	private float moveSpeed;
	public bool lockAxisChangeUntilStop = false;

	private Vector3 ?lastRecievedPosition;
	private Vector3 startingTilePosition;
	private Tile_Draggable selectedTile;
	private GridControl gridControl;
	private MovementPattern movementDummy;
	private List<Tile_Draggable> draggedTiles;


	public void Init(Tile_Draggable selectedTile, MovementType type, float speed = 6)
	{
		gridControl = selectedTile.ParentGrid;
		this.selectedTile = selectedTile;
		this.startingTilePosition = selectedTile.transform.position;
		movementAxis = Axis.None;
		draggedTiles = new List<Tile_Draggable>();

		var Dummy = new GameObject();
		switch(type)
		{
			case MovementType.Linear:
				movementDummy = Dummy.AddComponent<Linear>();
				break;
		}
		moveSpeed = speed;


		movementDummy.transform.position = selectedTile.transform.position;
	}

	public void SetPosition(Vector3 position, bool snapToGrid = false)
	{
		if (lockAxisChangeUntilStop)
		{
			if (!selectedTile.IsMoving)
				{
					lockAxisChangeUntilStop = false;
				}
				
			else
				return;
		}

		if (snapToGrid)
		{
			position = gridControl.grid.WorldToCell(position);
			var margin = gridControl.grid.cellSize / 2;
			margin.z = 0;
			position += margin;
		}

		lastRecievedPosition = position;

		previousAxis = movementAxis;
		UpdateMovementAxis(position);

		// This check is needed to avoid needlessly getting same tiles
		if (previousAxis != movementAxis && movementAxis != Axis.None)
		{
			for (int i = 0; i < draggedTiles.Count; i++)
			{
				Tile_Draggable T = draggedTiles[i];
				T.Stop();
			}
			draggedTiles = gridControl.GetTilesChain(selectedTile, movementAxis, currentDirection, this);
		}
			
		if (movementAxis == Axis.None && selectedTile.IsMoving)
		{
			lockAxisChangeUntilStop = true;
		}
	}

	public Axis UpdateMovementAxis(Vector3 moverPosition)
	{
		var relativePos = startingTilePosition - moverPosition;
		// to reset axis when we near starting point
		if (relativePos.magnitude < gridControl.grid.cellSize.magnitude / 2)
		{
			movementAxis = Axis.None;
		}
		else if (movementAxis == Axis.None)
		{
			if (Mathf.Abs(relativePos.x) >= gridControl.grid.cellSize.x / 2)
			{
				movementAxis = Axis.X;
			}
			else if (Mathf.Abs(relativePos.y) >= gridControl.grid.cellSize.y / 2)
			{
				movementAxis = Axis.Y;
			}
		}
		UpdateDirection(moverPosition);
		return movementAxis;
	}

	private void UpdateDirection(Vector3 position)
	{
		float distance = 0;
		switch (movementAxis)
		{
			case Axis.X:
				distance = position.x - startingTilePosition.x;
				if (distance > 0)
					currentDirection = Direction.Right;
				else if (distance < 0)
					currentDirection = Direction.Left;
				else
					currentDirection = Direction.None;
				break;
			case Axis.Y:
				distance = position.y - startingTilePosition.y;
				if (distance > 0)
					currentDirection = Direction.Top;
				else if(distance < 0)
					currentDirection = Direction.Bottom;
				else
					currentDirection = Direction.None;
				break;
			case Axis.None:
				currentDirection = Direction.None;
				break;
		}
	}

	/*
	 * Rework Plan
	 * 
	 * TODO: 
	 * 
	 */



	
	void Update()
	{
		
		if (!lastRecievedPosition.HasValue)
		{
			IsMoving = false;
			return;
		}
			
		if (draggedTiles.Count == 0)
		{
			IsMoving = false;
			return;
		}

		relativePos = lastRecievedPosition.Value - movementDummy.transform.position;

		distance = 0;

		switch (movementAxis)
		{
			case Axis.X:
				relativePos.y = 0;
				distance = relativePos.x;
				break;
			case Axis.Y:
				relativePos.x = 0;
				distance = relativePos.y;
				break;
			case Axis.None:
				relativePos = startingTilePosition - movementDummy.transform.position;
				switch (previousAxis)
				{
					case Axis.X:
						distance = relativePos.x;
						break;
					case Axis.Y:
						distance = relativePos.y;
						break;
				}
				break;
		}
		
		if (distance == 0)
		{
			IsMoving = false;
			return;
		}
		

		IsMoving = true;
		//movementDummy.Move(relativePos, moveSpeed);
		var movedAmount = movementDummy.Move(relativePos, moveSpeed).magnitude;
		if (movedAmount >= 0.5f)
			Debug.LogError(movedAmount);
		for (int i = 0; i < draggedTiles.Count; i++)
		{
			Tile_Draggable T = draggedTiles[i];
			T.DragMoveTo<Linear>(distance, moveSpeed);

		}
		/*
		if(movementAxis == Axis.None && selectedTile.transform.position.Approximately(startingTilePosition))
		{
			for (int i = 0; i < draggedTiles.Count; i++)
			{
				TileBase T = draggedTiles[i];
				T.Stop();
			}
			IsMoving = false;
		}*/

	}
	private IEnumerator FinalizeDrag()
	{
		yield return new WaitForEndOfFrame();
		while (IsMoving)
		{
			yield return null;
		}
		for (int i = 0; i < draggedTiles.Count; i++)
		{
			Tile_Draggable T = draggedTiles[i];
			T.SnapToGrid();
		}
		yield return new WaitForFixedUpdate();
		for (int i = 0; i < draggedTiles.Count; i++)
		{
			Tile_Draggable T = draggedTiles[i];
			T.Stop();
			T.FinishedMovement();

		}
		Destroy(this);
		yield break;
	}


	public void FinalizeMovement()
	{
		StartCoroutine(FinalizeDrag());
	}

	public TileBase GetMainTile()
	{
		return selectedTile;
	}

	public Axis GetMovementAxis()
	{
		return movementAxis;
	}

	private void OnDestroy()
	{
		Destroy(movementDummy.gameObject);
	}
}

