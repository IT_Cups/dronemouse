﻿using System;
using UnityEngine;


public class TileBase : MonoBehaviour, IMovable, ITiles.ITile
{

	public GridControl ParentGrid { get; set; }

	public bool IsMoving { get => movementPattern ? true : false; }

	public virtual bool Redirects { get; set; } = false;

	protected MovementPattern movementPattern;

	private SpriteRenderer spriteRenderer;

	public SpriteRenderer SpriteRenderer {
		get => spriteRenderer ? spriteRenderer : spriteRenderer = GetComponent<SpriteRenderer>();
		set => spriteRenderer = value;
	}
	

	// Start is called before the first frame update
	protected virtual void Awake()
    {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	// Update is called once per frame
	protected virtual void Update()
    {

	}

	protected virtual void Reset()
	{
	
	}

	// returns cell postiton of connected tile, null if already connected
	// if stealthy is true connecting tile wont trigger CellContentChanged() for every tile in that cell
	public virtual Vector3Int? ConnectToGrid(GridControl grid, bool stealthy = false)
	{
		return grid.ConnectTile(this, stealthy);
	}

	public virtual bool DisconnectFromGrid()
	{
		return ParentGrid.DisconnectTile(this);
	}

	public virtual void CellContentChanged(TileBase culprit = null)
	{
		return;
	}

	public virtual Vector3Int? FinishedMovement()
	{
		// RESUME: SNAP TO GRID (consider adapting to multitile tiles)
		var newPos = ParentGrid.UpdateTilePosition(this);
		if(newPos != null)
		{

		}
		return newPos;
	}


	public virtual Vector3 Move<T>(Vector3 vector, float maxSpeed) where T : MovementPattern, new()
	{
		Vector3 returnValue;
		if (movementPattern == null || !(movementPattern is T))
		{
			movementPattern = GetComponent<T>();
			if (movementPattern == null)
				movementPattern = gameObject.AddComponent<T>();
			returnValue = movementPattern.Move(vector, maxSpeed);
			return returnValue;
		}

		returnValue = movementPattern.Move(vector, maxSpeed);
		return returnValue;
	}

	public virtual Vector3Int GetInGridPosition()
	{
		return ParentGrid.GetTilePosition(this).Value;
	}

	public virtual Vector3 GetInGridWorldPosition()
	{
		Vector3Int gridPos = ParentGrid.GetTilePosition(this).Value;
		Vector3 pos = new Vector3(gridPos.x + ParentGrid.grid.cellSize.x / 2, gridPos.y + ParentGrid.grid.cellSize.y / 2, transform.position.z);
		return ParentGrid.grid.LocalToWorld(pos);
	}

	// if force is true will return tiles like teleporters instead of tiles on teleporter exits
	public virtual T GetAdjacentTile<T>(ref Axis axis, ref Direction direction, bool ignoreRedirect = false) where T : TileBase
	{
		Vector3Int? curPos = GetInGridPosition();
		T returnTile = null;
		switch (axis)
		{
			case Axis.X:
				returnTile = ParentGrid.GetTile<T>(new Vector3Int(curPos.Value.x + Math.Sign((int)direction), curPos.Value.y, 0));
				break;

			case Axis.Y:
				returnTile = ParentGrid.GetTile<T>(new Vector3Int(curPos.Value.x, curPos.Value.y + Math.Sign((int)direction), 0));
				break;
		}

		if(!ignoreRedirect)
		{
			if (returnTile && returnTile.Redirects)
			{
				returnTile = returnTile.GetAdjacentTile<T>(ref axis, ref direction, ignoreRedirect);
			}
			// let check if next tile redirects to T type
			else if (!returnTile)
			{
				var redirectTile = GetAdjacentTile<TileBase>(axis, direction, true);

				if (redirectTile && redirectTile.Redirects)
					returnTile = redirectTile.GetAdjacentTile<T>(ref axis, ref direction, ignoreRedirect);
			}
		}
		
			

		return returnTile;
	}
	public virtual T GetAdjacentTile<T>(Axis axis, Direction direction, bool ignoreRedirect = false) where T : TileBase
	{
		return GetAdjacentTile<T>(ref axis, ref direction, ignoreRedirect);
	}

	public virtual MovementError CanMove()
	{
		MovementError returnValue = MovementError.None;
		if (IsMoving)
			returnValue |= MovementError.BusyMoving;
		return returnValue;
	}

	public virtual void Stop()
	{
		movementPattern = null;
	}

	public virtual void SnapToGrid()
	{
		Vector3Int cellPosition = (Vector3Int)ParentGrid.grid.WorldToCell(transform.position);
		var margin = ParentGrid.grid.cellSize / 2;
		margin.z = 0;
		Vector3 snappedPos = cellPosition + margin;
		transform.position = snappedPos;
	}

}
