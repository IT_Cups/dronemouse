﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileBG : TileBase
{

	public override MovementError CanMove()
	{
		return MovementError.Immobile;
	}

}
