﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileBG_VictoryConditionColor : TileBG, ICondition
{
    public Color RequiredColor;
    private bool isMetFlag = false;

    private void OnValidate()
    {
        SpriteRenderer.color = RequiredColor * 0.5f;
    }
    public void AfterLostActions()
    {
        SpriteRenderer.color = RequiredColor * 0.5f;
    }

    public void AfterMetActions()
    {
        SpriteRenderer.color = RequiredColor * 0.8f;
        IVictoryController controller = ParentGrid as IVictoryController;
        if (controller != null)
            controller.CheckVictoryConditions();
    }

    public override void CellContentChanged(TileBase culprit = null)
    {
        var beforeIsMetFlag = isMetFlag;
        IsMet();
        if(beforeIsMetFlag != isMetFlag)
        {
            if (isMetFlag == true)
                AfterMetActions();
            else if (isMetFlag == false)
                AfterLostActions();
        }    
           
    }

    public bool IsMet()
    {
        var tileOnTop = ParentGrid.GetTile<Tile_Draggable>(this.GetInGridPosition());
        if (tileOnTop != null && tileOnTop.GetColor() == RequiredColor)
        {
            isMetFlag = true;
            return true;
        }
        isMetFlag = false;
        return false;
    }
}
