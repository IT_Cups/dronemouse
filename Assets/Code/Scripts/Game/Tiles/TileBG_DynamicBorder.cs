﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileBG_DynamicBorder : TileBG, ICosmetic, IDynamicSprite
{
	private DynamicTexture_SideConditions generator;
	public DynamicTexture_SideConditions Generator
	{
		get
		{
			if (generator)
				return generator;
			generator = GetComponentInParent<DynamicTexture_SideConditions>();
			return generator;
		}
		set => generator = value;
	}

	public void GenerateDynamicSprite(bool supressMessage = false)
	{
		Generator.GenerateSprite(supressMessage);
	}
}
