﻿using System;
using UnityEngine;

public class Tile_Draggable : TileBase, ITeleportable, ITiles.ITileDraggable
{

	public new bool IsMoving { get => MasterTileDrag ? MasterTileDrag.IsMoving : false; }

	public Axis MovementAxis { get; private set; } = Axis.None;
	public Direction DirectionOfMovement { get; private set; } = Direction.None;
	public bool Inversed { get; private set; } = false;
	public TileDrag MasterTileDrag { get; private set; } = null;

	public TeleportGhost TeleportationGhost { get; set; }
	public bool IsBeingTeleported => TeleportationGhost ? true : false;

	public override Vector3 Move<T>(Vector3 vector, float maxSpeed)
	{
		var returnValue = base.Move<T>(vector,maxSpeed);
		if (TeleportationGhost && returnValue != Vector3.zero)
			TeleportationGhost.UpdatePosition();

		return returnValue;
	}

	public virtual void DragMoveTo<T>(float distance, float maxSpeed) where T : MovementPattern, new()
	{
		if (!MasterTileDrag)
			Debug.LogError("Wrong usage of DragMoveTo()");

		if (Inversed)
			distance *= -1;

		Vector3 relativeVector = Vector3.zero;

		switch (MovementAxis)
		{
			case Axis.X:
				if (distance > 0)
					DirectionOfMovement = Direction.Right;
				else if (distance < 0)
					DirectionOfMovement = Direction.Left;
				else
					DirectionOfMovement = Direction.None;
				relativeVector = new Vector3(distance, 0, 0);
				break;
			case Axis.Y:
				if (distance > 0)
					DirectionOfMovement = Direction.Top;
				else if (distance < 0)
					DirectionOfMovement = Direction.Bottom;
				else
					DirectionOfMovement = Direction.None;
				relativeVector = new Vector3(0, distance, 0);
				break;
		}
		Move<T>(relativeVector, maxSpeed);

	}

	public virtual void SetDragRules(TileDrag master, Axis? axis, bool? isInversed)
	{
		if (master != null)
			MasterTileDrag = master;
		if (axis.HasValue)
			MovementAxis = axis.Value;
		if (isInversed.HasValue)
			Inversed = isInversed.Value;
	}

	public virtual Color GetColor()
	{
		return SpriteRenderer.color;
	}

	public virtual bool CanBeTeleported()
	{
		if (!IsBeingTeleported)
			return true;
		return false;
	}

	public override void Stop()
	{
		base.Stop();
		MovementAxis = Axis.None;
		MasterTileDrag = null;
		Inversed = false;
		DirectionOfMovement = Direction.None;
	}

	public override MovementError CanMove()
	{
		MovementError returnValue = base.CanMove();
		if (MasterTileDrag)
			returnValue |= MovementError.StopsOthers;
		return returnValue;
	}

	private void OnDestroy()
	{
		Destroy(TeleportationGhost);
	}

	public override void SnapToGrid()
	{
		base.SnapToGrid();
		TeleportationGhost?.UpdatePosition();
	}
}
