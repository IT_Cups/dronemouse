﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;
using MyBox;


public class DynamicTexture_GridSides : DynamicTexture_SideConditions
{
	private GridControl grid;
	private TileBase tile;

	public GridControl Grid
	{
		get => grid ? grid : grid = GetComponentInParent<GridControl>();
		set => grid = value;
	}

	public TileBase Tile
	{
		get => tile ? tile : tile = GetComponentInParent<TileBase>();
		set => tile = value;
	}

	// Start is called before the first frame update
	void Awake()
	{
		Destroy(this);
	}

	protected override bool CheckTop()
	{
		if (CheckTiles(Tile.GetInGridPosition() + Vector3Int.up))
			return true;
		return false;
	}
	protected override bool CheckRight()
	{
		if (CheckTiles(Tile.GetInGridPosition() + Vector3Int.right))
			return true;
		return false;
	}
	protected override bool CheckBottom()
	{
		if (CheckTiles(Tile.GetInGridPosition() + Vector3Int.down))
			return true;
		return false;
	}
	protected override bool CheckLeft()
	{
		if (CheckTiles(Tile.GetInGridPosition() + Vector3Int.left))
			return true;
		return false;
	}
	protected override bool CheckTopRight()
	{
		if (CheckTiles(Tile.GetInGridPosition() + Vector3Int.up + Vector3Int.right))
			return true;
		return false;
	}
	protected override bool CheckTopLeft()
	{
		if (CheckTiles(Tile.GetInGridPosition() + Vector3Int.up + Vector3Int.left))
			return true;
		return false;
	}
	protected override bool CheckBottomRight()
	{
		if (CheckTiles(Tile.GetInGridPosition() + Vector3Int.down + Vector3Int.right))
			return true;
		return false;
	}
	protected override bool CheckBottomLeft()
	{
		if (CheckTiles(Tile.GetInGridPosition() + Vector3Int.down + Vector3Int.left))
			return true;
		return false;
	}

	public override void GenerateSprite(bool supressMessage = false)
	{
		Grid.Validate(Tile);
		Grid.UpdateTilePosition(Tile);
		base.GenerateSprite(supressMessage);
	}

	private bool CheckTiles(Vector3Int pos)
	{
		List<TileBase> tiles = Grid.GetTiles(pos);
		if (tiles == null)
			return false;
		foreach (var t in tiles)
		{
			if (t is ITiles.ITileDraggable)
				return true;
		}
		return false;
	}
}
