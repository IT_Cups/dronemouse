﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;
using MyBox;


[RequireComponent(typeof(SpriteRenderer))]
public class DynamicTexture_SideConditions : MonoBehaviour
{
	public enum MergingType
	{
		Adding,
		Replacing
	}
	[SearchableEnum]
	public MergingType textureCombiningType = MergingType.Adding;
	[ConditionalField("textureCombiningType", false, MergingType.Adding)]
	[PreviewTexture]
	public Texture2D filling;
	[PreviewTexture]
	public Texture2D up;
	[PreviewTexture]
	public Texture2D right;
	[PreviewTexture]
	public Texture2D down;
	[PreviewTexture]
	public Texture2D left;
	[PreviewTexture]
	public Texture2D corners;
	public uint cornersCutMargin;
	[PreviewTexture]
	public Texture2D innerCorners;
	public uint innerCornersCutMargin;

	private Texture2D generatedTexture;
	//private SpriteRenderer renderer;
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	protected virtual bool CheckTop()
	{
		throw new NotImplementedException("Use existing or create class with overrides for checks.");
	}
	protected virtual bool CheckRight()
	{
		throw new NotImplementedException("Use existing or create class with overrides for checks.");
	}
	protected virtual bool CheckBottom()
	{
		throw new NotImplementedException("Use existing or create class with overrides for checks.");
	}
	protected virtual bool CheckLeft()
	{
		throw new NotImplementedException("Use existing or create class with overrides for checks.");
	}
	protected virtual bool CheckTopRight()
	{
		throw new NotImplementedException("Use existing or create class with overrides for checks.");
	}
	protected virtual bool CheckTopLeft()
	{
		throw new NotImplementedException("Use existing or create class with overrides for checks.");
	}
	protected virtual bool CheckBottomRight()
	{
		throw new NotImplementedException("Use existing or create class with overrides for checks.");
	}
	protected virtual bool CheckBottomLeft()
	{
		throw new NotImplementedException("Use existing or create class with overrides for checks.");
	}


	public virtual void GenerateSprite(bool supressMessage = false)
	{
		
		bool fTop = CheckTop();
		bool fRight = CheckRight();
		bool fBottom = CheckBottom();
		bool fLeft = CheckLeft();
		bool fTopRight = CheckTopRight();
		bool fTopLeft = CheckTopLeft();
		bool fBottomRight = CheckBottomRight();
		bool fBottomLeft = CheckBottomLeft();

		generatedTexture = new Texture2D(up.width, up.height, TextureFormat.RGBA32, up.mipmapCount > 1 ? true : false);
		generatedTexture.wrapMode = up.wrapMode;
		generatedTexture.filterMode = up.filterMode;
		generatedTexture.Clear();
		
		

		if (textureCombiningType == MergingType.Adding && filling)
		{
			generatedTexture = filling;
		}
		
		if (fTop)
		{
			if (textureCombiningType == MergingType.Adding)
				generatedTexture = generatedTexture.Blend(up);
			else
				generatedTexture = generatedTexture.Replace(up);
		}
		
		if (fRight)
		{
			if (textureCombiningType == MergingType.Adding)
				generatedTexture = generatedTexture.Blend(right);
			else
				generatedTexture = generatedTexture.Replace(right);
		}
		
		if (fBottom)
		{
			if (textureCombiningType == MergingType.Adding)
				generatedTexture = generatedTexture.Blend(down);
			else
				generatedTexture = generatedTexture.Replace(down);
		}
		if (fLeft)
		{
			if (textureCombiningType == MergingType.Adding)
				generatedTexture = generatedTexture.Blend(left);
			else
				generatedTexture = generatedTexture.Replace(left);
		}
		
		if (fTopRight)
		{
			var cutRectOuter = new RectInt(corners.width - (int)cornersCutMargin, corners.height - (int)cornersCutMargin, (int)cornersCutMargin, (int)cornersCutMargin);
			var cutRectInner = new RectInt(innerCorners.width - (int)innerCornersCutMargin, innerCorners.height - (int)innerCornersCutMargin, (int)innerCornersCutMargin, (int)innerCornersCutMargin);

			if (fTop && fRight)
			{
				if (textureCombiningType == MergingType.Adding)
					generatedTexture = generatedTexture.Blend(corners, cutRectOuter, cutRectOuter);
				else
					generatedTexture = generatedTexture.Replace(corners, cutRectOuter, cutRectOuter);
			}
			else if (!fTop && !fRight)
			{
				if (textureCombiningType == MergingType.Adding)
					generatedTexture = generatedTexture.Blend(innerCorners, cutRectInner, cutRectInner);
				else
					generatedTexture = generatedTexture.Replace(innerCorners, cutRectInner, cutRectInner);
			}
			
		}
		if (fTopLeft)
		{
			var cutRectOuter = new RectInt(0, corners.height - (int)cornersCutMargin, (int)cornersCutMargin, (int)cornersCutMargin);
			var cutRectInner = new RectInt(0, innerCorners.height - (int)innerCornersCutMargin, (int)innerCornersCutMargin, (int)innerCornersCutMargin);

			if (fTop && fLeft)
			{
				if (textureCombiningType == MergingType.Adding)
					generatedTexture = generatedTexture.Blend(corners, cutRectOuter, cutRectOuter);
				else
					generatedTexture = generatedTexture.Replace(corners, cutRectOuter, cutRectOuter);
			}
			else if (!fTop && !fLeft)
			{
				if (textureCombiningType == MergingType.Adding)
					generatedTexture = generatedTexture.Blend(innerCorners, cutRectInner, cutRectInner);
				else
					generatedTexture = generatedTexture.Replace(innerCorners, cutRectInner, cutRectInner);
			}
		}
		if (fBottomRight)
		{
			var cutRectOuter = new RectInt(corners.width - (int)cornersCutMargin, 0, (int)cornersCutMargin, (int)cornersCutMargin);
			var cutRectInner = new RectInt(innerCorners.width - (int)innerCornersCutMargin, 0, (int)innerCornersCutMargin, (int)innerCornersCutMargin);

			if (fBottom && fRight)
			{
				if (textureCombiningType == MergingType.Adding)
					generatedTexture = generatedTexture.Blend(corners, cutRectOuter, cutRectOuter);
				else
					generatedTexture = generatedTexture.Replace(corners, cutRectOuter, cutRectOuter);
			}
			else if (!fBottom && !fRight)
			{
				if (textureCombiningType == MergingType.Adding)
					generatedTexture = generatedTexture.Blend(innerCorners, cutRectInner, cutRectInner);
				else
					generatedTexture = generatedTexture.Replace(innerCorners, cutRectInner, cutRectInner);
			}
		}
		if (fBottomLeft)
		{
			var cutRectOuter = new RectInt(0, 0, (int)cornersCutMargin, (int)cornersCutMargin);
			var cutRectInner = new RectInt(0, 0, (int)innerCornersCutMargin, (int)innerCornersCutMargin);

			if (fBottom && fLeft)
			{
				if (textureCombiningType == MergingType.Adding)
					generatedTexture = generatedTexture.Blend(corners, cutRectOuter, cutRectOuter);
				else
					generatedTexture = generatedTexture.Replace(corners, cutRectOuter, cutRectOuter);
			}
			else if (!fBottom && !fLeft)
			{
				if (textureCombiningType == MergingType.Adding)
					generatedTexture = generatedTexture.Blend(innerCorners, cutRectInner, cutRectInner);
				else
					generatedTexture = generatedTexture.Replace(innerCorners, cutRectInner, cutRectInner);
			}
		}
		
		GetComponent<SpriteRenderer>().sprite = Sprite.Create(generatedTexture, new Rect(0, 0, generatedTexture.width, generatedTexture.height), transform.localScale/2);
		if(!supressMessage)
			Debug.Log("Sprite generated.");
	}
	private void OnValidate()
	{
		if(corners)
		{
			if (cornersCutMargin == 0)
				cornersCutMargin = (uint)corners.width / 2;
			else if(cornersCutMargin > corners.width)
				cornersCutMargin = (uint)corners.width;
			
		}
		if(innerCorners)
		{
			if (innerCornersCutMargin == 0)
				innerCornersCutMargin = (uint)innerCorners.width / 2;
			else if (innerCornersCutMargin > innerCorners.width)
				innerCornersCutMargin = (uint)innerCorners.width;
		}
			
	}
}
#if UNITY_EDITOR // conditional compilation is not mandatory
[CustomEditor(typeof(DynamicTexture_SideConditions), true), CanEditMultipleObjects]
public class RuledTileTextureMultisidedEditor : Editor
{

	// OnInspector GUI
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		if (GUILayout.Button("GenerateSprite"))
		{
			DynamicTexture_SideConditions targetScript = (DynamicTexture_SideConditions)target;
			targetScript.GenerateSprite();
		}

	}
}
#endif

