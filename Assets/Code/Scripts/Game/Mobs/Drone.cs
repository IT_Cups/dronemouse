﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drone : MonoBehaviour
{
	private TargetingManager targeingManager;


	private GameObject target;
	// Start is called before the first frame update
	void Start()
    {
		targeingManager = GetComponent<TargetingManager>();
	}

    // Update is called once per frame
	void Update()
	{
		if (Input.GetButtonUp("Click"))
		{
			var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			target = targeingManager.NewTarget(mousePos);
			//movementHandler.MoveTo(target, stayAtPosition: true);
		}
	}
}
