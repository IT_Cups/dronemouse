﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollide : MonoBehaviour
{

    // Start is called before the first frame update
    void Awake()
    {
		if(!GetComponent<Collider2D>())
			Debug.LogError("WRONG USAGE DestroyOnCollide");
	}

	void OnCollisionEnter(Collision collision)
	{
		Destroy(gameObject);
	}
}
