﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace UnityEngine
{
	public enum Axis
	{
		None,
		X = 1 << 0,
		Y = 1 << 1,
		Z = 1 << 2
	}

	public enum Direction
	{
		None,
		Left = -1,
		Right = 1,
		Top = 2,
		Bottom = -2,
		Forward = 3,
		Backward = -3
	}

}

