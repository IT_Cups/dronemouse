﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace UnityEngine
{
	public class PreviewSpriteAttribute : PropertyAttribute
	{
		public PreviewSpriteAttribute() { }
	}
	public class PreviewTextureAttribute : PropertyAttribute
	{
		public PreviewTextureAttribute() { }
	}
}

[CustomPropertyDrawer(typeof(PreviewSpriteAttribute))]
public class SpritePreview : PropertyDrawer
{
	static bool alterBackgroundFlag = false;



	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		Color backgroundColor;
		if (alterBackgroundFlag)
			backgroundColor = new Color(0.1f, 0.1f, 0.1f, 0.2f);
		else
			backgroundColor = new Color(0.1f, 0.1f, 0.1f, 0.1f);
		EditorGUI.DrawRect(position, backgroundColor);
		alterBackgroundFlag = !alterBackgroundFlag;



		EditorGUI.BeginProperty(position, label, property);
		var labelPos = position;
		labelPos.x += 2f;
		labelPos.y += 32f;
		EditorGUI.PrefixLabel(labelPos, GUIUtility.GetControlID(FocusType.Passive), label, EditorStyles.boldLabel);

		property.objectReferenceValue = EditorGUI.ObjectField(position, "", property.objectReferenceValue, typeof(Sprite), false);
		
		
		EditorGUI.EndProperty();
	}
	
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return base.GetPropertyHeight(property, label) + 64f;
	}
}


[CustomPropertyDrawer(typeof(PreviewTextureAttribute))]
public class TexturePreview : PropertyDrawer
{
	static bool alterBackgroundFlag = false;



	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		Color backgroundColor;
		if (alterBackgroundFlag)
			backgroundColor = new Color(0.1f, 0.1f, 0.1f, 0.2f);
		else
			backgroundColor = new Color(0.1f, 0.1f, 0.1f, 0.1f);
		EditorGUI.DrawRect(position, backgroundColor);
		alterBackgroundFlag = !alterBackgroundFlag;



		EditorGUI.BeginProperty(position, label, property);
		var labelPos = position;
		labelPos.x += 2f;
		labelPos.y += 32f;
		EditorGUI.PrefixLabel(labelPos, GUIUtility.GetControlID(FocusType.Passive), label, EditorStyles.boldLabel);

		property.objectReferenceValue = EditorGUI.ObjectField(position, "", property.objectReferenceValue, typeof(Texture), false);


		EditorGUI.EndProperty();
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return base.GetPropertyHeight(property, label) + 64f;
	}
}


