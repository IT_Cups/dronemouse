﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// TeleportGhost needs fixedupdate to check collision to finilaze teleport 
public class TeleportGhost : MonoBehaviour
{
	public bool keepRotation;
	public ITeleportable OriginObject { get; private set; }
	// relative point from teleporter where originObject have entered
	public Vector3 EnterPoint { get; private set; }
	public ITeleporter EnterTeleport { get; private set; }
	public ITeleporter ExitTeleporter { get; private set; }


	public void Init(ITeleportable orgObject, Vector3 enter, ITeleporter teleStart, ITeleporter teleEnd, bool keepObjectRotation = true)
	{
		OriginObject = orgObject;
		OriginObject.TeleportationGhost = this;
		EnterPoint = enter;
		EnterTeleport = teleStart;
		ExitTeleporter = teleEnd;
		keepRotation = keepObjectRotation;
	}


	// Start is called before the first frame update
	void Start()
    {
		
    }

    // Update is called once per frame
    void Update()
    {
		//UpdatePosition();

	}
	public void UpdatePosition()
	{
		if (OriginObject == null)
		{
			Destroy(gameObject);
			return;
		}
			
		// first we will rotate teleporter enter point based on rotation of exit teleporter
		Vector3 teleporterExitPoint = ExitTeleporter.transform.rotation * (Quaternion.Inverse(EnterTeleport.transform.rotation) * EnterPoint);
		// then we apply relative teleporter enter position to connected teleporter to find proper exit teleporter position
		teleporterExitPoint = ExitTeleporter.gameObject.RelativePositionToWorld(teleporterExitPoint);
		//DebugHelper.DrawDebugCross(Color.yellow, teleporterExitPoint);
		// then we need to find obj position relative to teleporter enter point
		Vector3 ghostPos = EnterTeleport.gameObject.RelativePositionToWorld(EnterPoint) - OriginObject.transform.position;
		// and rotate it aswell
		ghostPos = ExitTeleporter.transform.rotation * (Quaternion.Inverse(EnterTeleport.transform.rotation) * ghostPos);
		// then apply relative pos to exit point and get ghost position
		transform.position = teleporterExitPoint.Round() + ghostPos.Round();
		if (keepRotation)
			this.transform.rotation = OriginObject.transform.rotation;

	}

	public ITeleportable FinalizeTeleport(bool failure = false)
	{
		if(failure)
		{
			EnterTeleport.TeleportingObjects.Remove(OriginObject.gameObject);
			ExitTeleporter.TeleportingGhosts.Remove(gameObject);

			OriginObject.TeleportationGhost = null;
			Destroy(gameObject);

			return null;
		}
		else
		{
			OriginObject.transform.position = transform.position;
			if (keepRotation)
				OriginObject.transform.rotation = transform.rotation;

			EnterTeleport.TeleportingObjects.Remove(OriginObject.gameObject);
			ExitTeleporter.TeleportingGhosts.Remove(gameObject);

			var returnValue = OriginObject;
			OriginObject.TeleportationGhost = null;
			Destroy(gameObject);
			return returnValue;
		}
		
	}

}
