﻿using UnityEngine;

public static class ExtensionsMisc
{
	// Direction
	public static Axis GetAxis(this Direction dir)
	{
		if (dir == Direction.Left || dir == Direction.Right)
			return Axis.X;
		else if (dir == Direction.Top || dir == Direction.Bottom)
			return Axis.Y;
		else
			return Axis.Z;
	}
	public static bool IsSameAxisAs(this Direction firstDir, Direction secondDir)
	{
		if (firstDir.GetAxis() == secondDir.GetAxis())
			return true;
		return false;
	}

	public static bool IsOpposite(this Direction thisDir, Direction otherDir)
	{
		if (thisDir != otherDir && (thisDir.IsSameAxisAs(otherDir)))
			return true;
		return false;
	}
	public static Direction GetOpposite(this Direction thisDir)
	{
		return (Direction)((int)thisDir * -1);
	}

	public static bool ExactAs(this RectInt thisRect, RectInt otherRect)
	{
		if(thisRect.x == otherRect.x && thisRect.y == otherRect.y && thisRect.width == otherRect.width && thisRect.height == otherRect.height)
			return true;
		return false;
	}
	public static bool SameDimensionsAs(this RectInt thisRect, RectInt otherRect)
	{
		if (thisRect.width == otherRect.width && thisRect.height == otherRect.height)
			return true;
		return false;
	}

	public static bool IsEmpty(this RectInt thisRect)
	{
		if (thisRect.x == 0 && thisRect.y == 0 && thisRect.width == 0 && thisRect.height == 0)
			return true;
		return false;
	}

} 
