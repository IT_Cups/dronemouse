﻿using UnityEngine;


public static class ExtensionsTexture
{
	// quality can drop due to generation of mipmaps
	// TODO: add support for different texture blending as well as pixel modes
	public static Texture2D Blend(this Texture2D thisTexture, Texture2D addTexture, RectInt destinationArea, RectInt sourceArea, ExtensionsColor.ColorBlendMode mode = ExtensionsColor.ColorBlendMode.Normal, bool trimExcess = true)
	{
		if (!destinationArea.SameDimensionsAs(sourceArea))
			throw new System.ArgumentException("Cut texure rectangle size have to be exact as destination rectangle size.");

		int newWidth = trimExcess ? thisTexture.width : Mathf.Max(thisTexture.width, (thisTexture.width + (destinationArea.x < 0 ? Mathf.Abs(destinationArea.x) : Mathf.Max(0, (Mathf.Abs(destinationArea.x) + destinationArea.width) - thisTexture.width))));
		int newHeight = trimExcess ? thisTexture.height : Mathf.Max(thisTexture.height, (thisTexture.height + (destinationArea.y < 0 ? Mathf.Abs(destinationArea.y) : Mathf.Max(0, (Mathf.Abs(destinationArea.y) + destinationArea.height) - thisTexture.height))));

		// first we create new textures with new size and place source textures with altered position
		Texture2D tThis = new Texture2D(newWidth, newHeight, TextureFormat.RGBA32, false);
		Texture2D tAdd = new Texture2D(newWidth, newHeight, TextureFormat.RGBA32, false);
		if(trimExcess)
		{
			tThis = thisTexture;
			tAdd.SetPixels(new Color[tAdd.width * tAdd.height]);
			RectInt cutArea = sourceArea;
			if (destinationArea.x < 0)
			{
				cutArea.x -= destinationArea.x;
				cutArea.width += destinationArea.x;
			}
			else
				cutArea.width -= destinationArea.x;
			if (destinationArea.y < 0)
			{
				cutArea.y -= destinationArea.y;
				cutArea.height += destinationArea.y;
			}
			else
				cutArea.height -= destinationArea.y;
			tAdd = tAdd.Replace(addTexture.Cut(cutArea), new RectInt(Mathf.Max(0, destinationArea.x), Mathf.Max(0, destinationArea.y), cutArea.width, cutArea.height), new RectInt(0,0, cutArea.width, cutArea.height));
		}
		else
		{
			tThis.SetPixels(new Color[tThis.width * tThis.height]);
			tThis = tThis.Replace(thisTexture, new RectInt(destinationArea.x < 0 ? Mathf.Abs(destinationArea.x) : 0, destinationArea.y < 0 ? Mathf.Abs(destinationArea.y) : 0, thisTexture.width, thisTexture.height), new RectInt(0, 0, thisTexture.width, thisTexture.height));
			tAdd.SetPixels(new Color[tAdd.width * tAdd.height]);
			tAdd = tAdd.Replace(addTexture.Cut(sourceArea), new RectInt(Mathf.Max(0, destinationArea.x), Mathf.Max(0, destinationArea.y), sourceArea.width, sourceArea.height), new RectInt(0, 0, sourceArea.width, sourceArea.height));
		}


		// Now we blend those textures together for final result
		// TODO: consider is mipmap needed at all
		Texture2D finalTexture = new Texture2D(newWidth, newHeight, TextureFormat.RGBA32, thisTexture.mipmapCount > 1 ? true : false);
		Color[] finalColors = new Color[newWidth * newHeight];

		finalColors = finalColors.Replace(tThis.GetPixels());
		finalColors = finalColors.Blend(tAdd.GetPixels(), mode);

		finalTexture.SetPixels(finalColors);
		finalTexture.wrapMode = thisTexture.wrapMode;
		finalTexture.filterMode = thisTexture.filterMode;
		finalTexture.Apply();
		return finalTexture;
	}
	public static Texture2D Blend(this Texture2D thisTexture, Texture2D addTexture, Vector2Int destinationArea, ExtensionsColor.ColorBlendMode mode = ExtensionsColor.ColorBlendMode.Normal, bool trimExcess = true)
	{
		int newWidth = trimExcess ? thisTexture.width : Mathf.Max(thisTexture.width, (thisTexture.width + (thisTexture.width - addTexture.width) + Mathf.Abs(destinationArea.x)));
		int newHeight = trimExcess ? thisTexture.height : Mathf.Max(thisTexture.height, (thisTexture.height + (thisTexture.height - addTexture.height) + Mathf.Abs(destinationArea.y)));
		// first we create new textures with new size and place source textures with altered position
		Color[] thisColors = trimExcess ? thisTexture.GetPixels() : thisTexture.OffsetTexture(-destinationArea, trimExcess).GetPixels();
		Color[] addColors = addTexture.OffsetTexture(destinationArea, trimExcess).GetPixels();
		// Now we blend those textures together for final result
		// TODO: consider is mipmap needed at all
		Texture2D finalTexture = new Texture2D(newWidth, newHeight, TextureFormat.RGBA32, thisTexture.mipmapCount > 1 ? true : false);
		Color[] finalColors = new Color[newWidth * newHeight];

		finalColors = finalColors.Replace(thisColors);
		finalColors = finalColors.Blend(addColors, mode);

		finalTexture.SetPixels(finalColors);
		finalTexture.wrapMode = thisTexture.wrapMode;
		finalTexture.filterMode = thisTexture.filterMode;
		finalTexture.Apply();
		return finalTexture;
	}

	public static Texture2D Blend(this Texture2D thisTexture, Texture2D addTexture, ExtensionsColor.ColorBlendMode mode = ExtensionsColor.ColorBlendMode.Normal)
	{
		if (thisTexture.width < addTexture.width || thisTexture.height < addTexture.height)
			throw new System.ArgumentException("Textures to blend have to be exact or less size than destination texture.");

		// TODO: consider is mipmap needed at all
		Texture2D finalTexture = new Texture2D(thisTexture.width, thisTexture.height, thisTexture.format, thisTexture.mipmapCount > 1 ? true : false);

		Color[] thisColors = thisTexture.GetPixels();
		Color[] addColors = addTexture.GetPixels();

		Color[] finalColors = thisColors.Blend(addColors, mode);
		finalTexture.SetPixels(finalColors);
		finalTexture.wrapMode = thisTexture.wrapMode;
		finalTexture.filterMode = thisTexture.filterMode;
		finalTexture.Apply();
		return finalTexture;
	}

	public static Texture2D Replace(this Texture2D thisTexture, Texture2D sourceTexture, RectInt destinationArea, RectInt sourceArea)
	{
		if (!destinationArea.SameDimensionsAs(sourceArea))
			throw new System.ArgumentException("Textures to replace have to be exact size as destination texture.");

		Texture2D finalTexture = new Texture2D(thisTexture.width, thisTexture.height, TextureFormat.RGBA32, thisTexture.mipmapCount > 1 ? true : false);
		finalTexture.SetPixels(thisTexture.GetPixels());
		Color[] replacedColors = sourceTexture.GetPixels(sourceArea.x, sourceArea.y, sourceArea.width, sourceArea.height);

		finalTexture.SetPixels(destinationArea.x, destinationArea.y, destinationArea.width, destinationArea.height, replacedColors);
		finalTexture.wrapMode = thisTexture.wrapMode;
		finalTexture.filterMode = thisTexture.filterMode;
		finalTexture.Apply();
		return finalTexture;
	}
	public static Texture2D Replace(this Texture2D thisTexture, Texture2D sourceTexture)
	{
		if (thisTexture.width < sourceTexture.width || thisTexture.height < sourceTexture.height)
			throw new System.ArgumentException("Textures to replace have to be exact or less size than destination texture.");

		Texture2D finalTexture = new Texture2D(thisTexture.width, thisTexture.height, TextureFormat.RGBA32, thisTexture.mipmapCount > 1 ? true : false);
		finalTexture.SetPixels(thisTexture.GetPixels());
		Color[] replacedColors = thisTexture.GetPixels().Replace(sourceTexture.GetPixels());

		finalTexture.SetPixels(replacedColors);
		finalTexture.wrapMode = thisTexture.wrapMode;
		finalTexture.filterMode = thisTexture.filterMode;
		finalTexture.Apply();
		return finalTexture;
	}

	public static Texture2D Cut(this Texture2D thisTexture, RectInt area, bool resize = true)
	{
		Texture2D cutTexture = new Texture2D(resize ? area.width : thisTexture.width, resize ? area.height : thisTexture.height, TextureFormat.RGBA32, false);
		if(resize)
			cutTexture.SetPixels(thisTexture.GetPixels(area.x, area.y, area.width, area.height));
		else
			cutTexture.SetPixels(area.x, area.y, area.width, area.height, thisTexture.GetPixels(area.x, area.y, area.width, area.height));
		cutTexture.wrapMode = thisTexture.wrapMode;
		cutTexture.filterMode = thisTexture.filterMode;
		cutTexture.Apply();
		return cutTexture;
	}

	public static Texture2D OffsetTexture(this Texture2D thisTexture, Vector2Int offset, bool trimExcess = true)
	{
		if (offset.magnitude == 0)
			return thisTexture;

		Texture2D final;

		if (!trimExcess)
		{
			int newWidth = thisTexture.width + Mathf.Abs(offset.x);
			int newHeight = thisTexture.height + Mathf.Abs(offset.y);
			final = new Texture2D(newWidth, newHeight)
			{
				wrapMode = thisTexture.wrapMode,
				filterMode = thisTexture.filterMode
			};
		}
		else
			final = new Texture2D(thisTexture.width, thisTexture.height, TextureFormat.RGBA32, thisTexture.mipmapCount > 1 ? true : false);

		final.Clear();

		if (trimExcess)
		{
			final.SetPixels(Mathf.Max(offset.x, 0), Mathf.Max(offset.y, 0), thisTexture.width - Mathf.Abs(offset.x), thisTexture.height - Mathf.Abs(offset.y), thisTexture.GetPixels(offset.x < 0 ? Mathf.Abs(offset.x) : 0, offset.y < 0 ? Mathf.Abs(offset.y) : 0, thisTexture.width - Mathf.Abs(offset.x), thisTexture.height - Mathf.Abs(offset.y)));
		}
		else
			final.SetPixels(offset.x > 0 ? Mathf.Abs(offset.x) : 0, offset.y > 0 ? Mathf.Abs(offset.y) : 0, thisTexture.width, thisTexture.height, thisTexture.GetPixels());
		final.Apply();
		return final;
	}
	public static void Clear(this Texture2D thisTexture)
	{
		thisTexture.SetPixels(new Color[thisTexture.width * thisTexture.height]);
		thisTexture.Apply();
	}


} 
