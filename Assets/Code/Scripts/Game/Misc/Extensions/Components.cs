﻿using UnityEngine;
using System;
using System.Reflection;

public static class ExtensionsComponents
{
	//Components
	public static dynamic GetCopyOf(this Component comp, dynamic other)
	{
		Type type = comp.GetType();
		Type otherType = other.GetType();
		if (!(type == otherType || otherType.IsSubclassOf(type)))
		{
			Debug.LogError("GetCopyOf: type mis-match");
			return null; // type mis-match
		}

		BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
		PropertyInfo[] pinfos = type.GetProperties(flags);
		foreach (var pinfo in pinfos)
		{
			if (pinfo.CanWrite)
			{
				try
				{
					pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
				}
				catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
			}
		}
		FieldInfo[] finfos = type.GetFields(flags);
		foreach (var finfo in finfos)
		{
			finfo.SetValue(comp, finfo.GetValue(other));
		}
		return comp;
	}
	
	public static dynamic AddComponent(this GameObject go, Component toAdd)
	{
		Type t = toAdd.GetType();
		return Convert.ChangeType(go.AddComponent(t).GetCopyOf(toAdd), t) ;
	}


} 
