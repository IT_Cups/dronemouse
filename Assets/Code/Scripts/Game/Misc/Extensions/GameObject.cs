﻿using System.Collections.Generic;
using UnityEngine;

public static class ExtensionsGameObject
{
	// GameObject
	public static Vector3 GetRelativePosition(this GameObject currentObj, GameObject targetObject)
	{
		return targetObject.transform.position - currentObj.transform.position;
	}
	public static Vector3 GetRelativePosition(this GameObject currentObj, Vector3 targetPosition)
	{
		return targetPosition - currentObj.transform.position;
	}
	public static float GetDistance(this GameObject currentObj, GameObject targetObject)
	{
		return Vector3.Distance(currentObj.transform.position, targetObject.transform.position);
	}
	public static float GetDistance(this GameObject currentObj, Vector3 targetPosition)
	{
		return Vector3.Distance(currentObj.transform.position, targetPosition);
	}

	public static Vector3 GetDirection(this GameObject currentObj, GameObject targetObject)
	{
		//return currentObj.transform.InverseTransformPoint(targetObject.transform.position);
		return currentObj.GetRelativePosition(targetObject) / currentObj.GetDistance(targetObject);
	}
	public static Vector3 GetDirection(this GameObject currentObj, Vector3 targetPosition)
	{
		//return currentObj.transform.InverseTransformPoint(targetPosition);
		return currentObj.GetRelativePosition(targetPosition) / currentObj.GetDistance(targetPosition);
	}

	public static GameObject[] FindGameObjectsWithLayer(this GameObject currentObj, int layer)
	{
		GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();
		List<GameObject> returnList = new List<GameObject>();

		for (var i = 0; i < allObjects.Length; i++)
		{
			if (allObjects[i].layer == layer)
			{
				returnList.Add(allObjects[i]);
			}
		}
		if (returnList.Count == 0)
		{
			return null;
		}
		return returnList.ToArray();
	}

	public static Vector3 RelativePositionToWorld(this GameObject currentObj, Vector3 relativePos)
	{
		return currentObj.transform.position + relativePos;
	}

	public static Target CreateNewScriptedTarget(this GameObject currentObj, Vector3 position)
	{
		var nTarget = new GameObject("Target of " + currentObj.name);
		nTarget.transform.position = position;
		
		var targetScript = nTarget.AddComponent<Target>();
		targetScript.Init(currentObj);
		return targetScript;
	}

	public static GameObject CreateNewTarget(this GameObject currentObj, Vector3 position)
	{
		var nTarget = new GameObject("Target of " + currentObj.name);
		nTarget.transform.position = position;

		return nTarget;
	}

	public static Direction GetSide(this GameObject thisObject, GameObject otherObject)
	{
		Vector3 direction = thisObject.GetDirection(otherObject);

		if(Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
		{
			if (Mathf.Abs(direction.x) > Mathf.Abs(direction.z))
			{
				if (direction.x > 0)
					return Direction.Right;
				else
					return Direction.Left;
			}
			else
			{
				if (direction.z > 0)
					return Direction.Forward;
				else
					return Direction.Backward;
			}
		}
		else if(Mathf.Abs(direction.y) > Mathf.Abs(direction.z))
		{
			if (direction.y > 0)
				return Direction.Top;
			else
				return Direction.Bottom;
		}
		else
		{
			if (direction.z > 0)
				return Direction.Forward;
			else
				return Direction.Backward;
		}
	}

} 
