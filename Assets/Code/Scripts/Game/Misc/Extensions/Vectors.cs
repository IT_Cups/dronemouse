﻿using UnityEngine;

public static class ExtentionsVector
{
	// Vectors
	public static Vector3 Round(this Vector3 thisVector, int decimalPlaces = 2)
	{
		float multiplier = 1;
		for (int i = 0; i < decimalPlaces; i++)
		{
			multiplier *= 10f;
		}
		return new Vector3(
			Mathf.Round(thisVector.x * multiplier) / multiplier,
			Mathf.Round(thisVector.y * multiplier) / multiplier,
			Mathf.Round(thisVector.z * multiplier) / multiplier);
	}
	public static bool Approximately(this Vector3 thisVector, Vector3 otherVector)
	{
		if (Mathf.Approximately(thisVector.x, otherVector.x) && Mathf.Approximately(thisVector.y, otherVector.y) && Mathf.Approximately(thisVector.z, otherVector.z))
			return true;
		else
			return false;
	}

} 
