﻿using UnityEngine;
using System.Collections;

public static class DebugHelper
{
	public static void DrawGizmoArrow(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
	{
		Gizmos.color = color;
		Gizmos.DrawRay(pos, direction);

		Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
		Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
		Gizmos.DrawRay(pos + direction, right * arrowHeadLength);
		Gizmos.DrawRay(pos + direction, left * arrowHeadLength);
	}
	/*
	public static void DrawDebugArrow(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
	{
		Debug.DrawRay(pos, direction, color);

		Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
		Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
		Debug.DrawRay(pos + direction, right * arrowHeadLength, color);
		Debug.DrawRay(pos + direction, left * arrowHeadLength, color);
	}*/
	public static void DrawDebugArrow(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
	{
		if (direction.magnitude < 0.01)
			return;
		Debug.DrawRay(pos, direction, color);

		Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * (direction / 5);
		Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * (direction / 5);
		Vector3 up = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 0, 180 + arrowHeadAngle) * (direction / 5);
		Vector3 down = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 0, 180 - arrowHeadAngle) * (direction / 5);
		Debug.DrawRay(pos + direction, right * arrowHeadLength, color);
		Debug.DrawRay(pos + direction, left * arrowHeadLength, color);
		Debug.DrawRay(pos + direction, up * arrowHeadLength, color);
		Debug.DrawRay(pos + direction, down * arrowHeadLength, color);
	}

	public static void DrawGizmoCross(Color color, Vector3 position, float radius = 0.5f)
	{
		Gizmos.color = color;
		Gizmos.DrawLine(new Vector3(position.x - radius, position.y, position.z - radius), new Vector3(position.x + radius, position.y, position.z + radius));
		Gizmos.DrawLine(new Vector3(position.x, position.y - radius, position.z - radius), new Vector3(position.x, position.y + radius, position.z + radius));
		Gizmos.DrawLine(new Vector3(position.x - radius, position.y, position.z), new Vector3(position.x + radius, position.y, position.z));
	}

	public static void DrawDebugCross(Color color, Vector3 position, float radius = 0.5f)
	{
		Debug.DrawLine(new Vector3(position.x - radius, position.y, position.z - radius), new Vector3(position.x + radius, position.y, position.z + radius), color);
		Debug.DrawLine(new Vector3(position.x, position.y - radius, position.z - radius), new Vector3(position.x, position.y + radius, position.z + radius), color);
		Debug.DrawLine(new Vector3(position.x - radius, position.y, position.z), new Vector3(position.x + radius, position.y, position.z), color);
	}

}