﻿using UnityEngine;


public interface ITeleportable : IGameObject
{
	TeleportGhost TeleportationGhost { get; set;}
	bool IsBeingTeleported { get; }
	bool CanBeTeleported();

}
