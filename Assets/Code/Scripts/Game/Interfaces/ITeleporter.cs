﻿using UnityEngine;
using System.Collections.Generic;


public interface ITeleporter : IGameObject
	// use TeleporterDive or simular with this
{
	ITeleporter ConnectedTeleporter { get; set;}
	Dictionary<GameObject, ITeleportable> TeleportingObjects { get; set; }
	Dictionary<GameObject, TeleportGhost> TeleportingGhosts { get; set; }

	// This only applicable to 2D space (TODO: separate in additional interface) 
	Direction FacingDirection { get; set; }

	bool ConnectTeleporter(ITeleporter tele);
	void ProcessTeleporting();
	bool CanTeleportObject(GameObject obj);
	bool InitiateTeleport(ITeleportable obj, Vector3 enterPoint);
	ITeleportable FinalizeTeleport(GameObject obj);


}