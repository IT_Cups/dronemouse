﻿using UnityEngine;

public interface IUIElement : IHideble
{
	string id { get; }

}
