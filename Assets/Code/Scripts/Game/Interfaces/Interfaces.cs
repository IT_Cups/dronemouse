﻿using UnityEngine;

public interface IGameObject
{
#pragma warning disable IDE1006 // Naming Styles
	GameObject gameObject { get; }
	Transform transform { get; }
#pragma warning restore IDE1006 // Naming Styles
}
