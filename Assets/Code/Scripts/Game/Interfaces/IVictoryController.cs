﻿using System.Collections.Generic;
using UnityEngine;

public interface IVictoryController
{
	/// <summary>
	/// List of conditions that needs to be fulfilled before victory can be achieved.
	/// </summary>
	List<ICondition> VictoryConditions { get; set; }
	/// <summary>
	/// Actions that should be taken after all conditions are met.
	/// </summary>
	void VictoryActions();
	/// <summary>
	/// Check conditions and calls <seealso cref="VictoryActions()"/> if all conditions are met.
	/// </summary>
	void CheckVictoryConditions();

}
