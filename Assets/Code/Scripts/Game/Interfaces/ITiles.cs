﻿using UnityEngine;
using System.Collections;

namespace ITiles
{
	public interface ITile : IGameObject
	{

		GridControl ParentGrid { get; set; }
		Vector3Int GetInGridPosition();
		Vector3 GetInGridWorldPosition();
		T GetAdjacentTile<T>(ref Axis axis, ref Direction direction, bool ignoreRedirect = false) where T : TileBase;


	}

	public interface ITileDraggable : IGameObject
	{

		Axis MovementAxis { get; }
		Direction DirectionOfMovement { get; }
		bool Inversed { get; }
		TileDrag MasterTileDrag { get; }

		void DragMoveTo<T>(float distance, float maxSpeed) where T : MovementPattern, new();

		void SetDragRules(TileDrag master, Axis? axis, bool? isInversed);

	}
}


