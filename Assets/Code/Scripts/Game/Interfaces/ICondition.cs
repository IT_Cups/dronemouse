﻿using UnityEngine;

public interface ICondition : IGameObject
{
	/// <summary>
	/// Returns true if conditions are fulfilled.
	/// </summary>
	bool IsMet();
	/// <summary>
	/// Actions that should be taken after conditions are met.
	/// </summary>
	void AfterMetActions();
	/// <summary>
	/// Actions that should be taken after conditions are lost.
	/// </summary>
	void AfterLostActions();

}
