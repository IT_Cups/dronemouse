﻿using UnityEngine;

public interface IHideble : IGameObject
{
	/// <summary>
	/// Bool property that indicates whether element shown or not.
	/// </summary>
	bool IsHidden { get; }
	/// <summary>
	/// Hides element from view.
	/// </summary>
	void Hide();
	/// <summary>
	/// Shows element.
	/// </summary>
	void Show();


}
