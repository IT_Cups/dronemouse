﻿using UnityEngine;
using System.Collections;
[System.Flags]
public enum MovementError : uint
{
    None = 0,
    BusyMoving = 1 << 1,
    Immobile = 1 << 2,
    StopsOthers = 1 << 3
}

public interface IMovable : IGameObject
{
    
    bool IsMoving { get;}

    MovementError CanMove();
	Vector3 Move<T>(Vector3 vector, float maxSpeed) where T : MovementPattern, new();
	void Stop();

}


