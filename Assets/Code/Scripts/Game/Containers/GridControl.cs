﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GridControl : MonoBehaviour
{
	// Tile data organized by type
	// two dimensional dictionary, first dictionary uses key as a Type of a tile that gonna be stored, second dictionary is different for each field
	// tilesByPosition : second dictionary key is a position in grid and value is list of tiles on that pos
	protected Dictionary<Type, Dictionary<Vector3Int, List<TileBase>>> tilesByPosition = new Dictionary<Type, Dictionary<Vector3Int, List<TileBase>>>();
	// positionByTiles : second dictionary key is a tile and its value is position of that tile
	protected Dictionary<Type, Dictionary<TileBase, Vector3Int>> positionByTiles = new Dictionary<Type, Dictionary<TileBase, Vector3Int>>();

	public Grid grid;
	public TileBase debugTile;
    // Start is called before the first frame update
    protected virtual void Start()
    {
		PopulateTileList();
	}

	protected virtual void Awake()
	{

	}

	// Only use once in runtime
	// There is no (protected method/friend class) thing in C#
	private void PopulateTileList()
	{
		grid = GetComponent<Grid>();
		List<TileBase> tileScripts = new List<TileBase>(gameObject.GetComponentsInChildren<TileBase>());

		HashSet<Vector3Int> cellsToAgitate= new HashSet<Vector3Int>();
		foreach (var V in tileScripts)
		{
			var cellPos = V.ConnectToGrid(this, true);
			if (cellPos != null && !cellsToAgitate.Contains(cellPos.Value))
				cellsToAgitate.Add(cellPos.Value);
		}
		foreach (var cell in cellsToAgitate)
		{
			AgitateCellContent(cell);
		}

	}

	// this two methods are mostly used in editor to populate tile dictionaries  
	public void Validate()
	{
		if (positionByTiles == null || positionByTiles.Count == 0)
			PopulateTileList();
	}

	public void Validate(TileBase tile)
	{
		if (positionByTiles == null || positionByTiles.Count == 0)
			PopulateTileList();

		if (tile.transform.parent.GetComponent<GridControl>() != this)
		{
			Debug.LogError("Can't validate tile to this grid, tile is not inside grid. " + tile.transform.parent.name + gameObject.name);
			return;
		}
		if (tile.ParentGrid != this)
		{
			tile.DisconnectFromGrid();
		}
		if(tile.ParentGrid == null)
		{
			tile.ConnectToGrid(this);
		}
	}

	// returns cell postiton of connected tile, null if already connected
	// if stealthy is true connecting tile wont trigger CellContentChanged() for every tile in that cell
	public virtual Vector3Int? ConnectTile(TileBase tile, bool stealthy = false)
    {
		if (positionByTiles.ContainsKey(tile.GetType()) && positionByTiles[tile.GetType()].ContainsKey(tile))
			return null;
		
		Vector3Int cellPosition = (Vector3Int)grid.WorldToCell(tile.transform.position);

		if (!positionByTiles.ContainsKey(tile.GetType()))
			positionByTiles[tile.GetType()] = new Dictionary<TileBase, Vector3Int>();

		positionByTiles[tile.GetType()][tile] = cellPosition;

		if (!tilesByPosition.ContainsKey(tile.GetType()))
			tilesByPosition[tile.GetType()] = new Dictionary<Vector3Int, List<TileBase>>();

		if (!tilesByPosition[tile.GetType()].ContainsKey(cellPosition))
			tilesByPosition[tile.GetType()][cellPosition] = new List<TileBase>();

		List<TileBase> tiles = tilesByPosition[tile.GetType()][cellPosition];
		tiles.Add(tile);	

		tile.ParentGrid = this;
		if (!stealthy)
			AgitateCellContent(cellPosition, tile);

		return cellPosition;
    }

	//returns false if tile is not found
	public bool DisconnectTile(TileBase tile)
	{
		if (!positionByTiles.ContainsKey(tile.GetType()) || !positionByTiles[tile.GetType()].ContainsKey(tile))
			return false;

		Vector3Int position = positionByTiles[tile.GetType()][tile];
		positionByTiles[tile.GetType()].Remove(tile);

		List<TileBase> tiles = tilesByPosition[tile.GetType()][position];
		tiles.Remove(tile);

		tile.ParentGrid = null;
		return true;
	}

	public Vector3Int? UpdateTilePosition(TileBase tile)
	{
		if (!positionByTiles.ContainsKey(tile.GetType()) || !positionByTiles[tile.GetType()].ContainsKey(tile))
		{
			Debug.LogError("Tile was not found in grid");
			return null;
		}
		Vector3Int newPosition = (Vector3Int)grid.WorldToCell(tile.transform.position);
		// if position is the same, do nothing
		if (positionByTiles[tile.GetType()][tile] == newPosition)
			return null;

		DisconnectTile(tile);
		ConnectTile(tile);
		return newPosition;
	}

	public void DestroyTile(TileBase tile)
	{
		if (!DisconnectTile(tile))
			return;
		Destroy(tile);
	}
	
	public void DestroyTiles<T>(Vector3Int position) where T : TileBase
	{
		foreach(var tile in GetTiles(position))
		{
			if(tile is T)
				DestroyTile(tile);
		}
	}

	public void DestroyTiles(Vector3Int position)
	{
		foreach (var tile in GetTiles(position))
		{
			DestroyTile(tile);
		}
	}

	///returns last tile in given position
	public T GetTile<T>(Vector3Int position) where T : TileBase
	{
		List<T> tiles = GetTiles<T>(position);
		if (tiles?.Count > 0)
			return tiles[tiles.Count - 1];
		return null;
	}

	/// returns last tile
	public TileBase GetTile(Vector3Int position)
	{
		List<TileBase> tiles = GetTiles(position);

		if (tiles?.Count > 0)
			return tiles[tiles.Count - 1];
		return null;
	}

	//returns all tiles in given position
	public List<T> GetTiles<T>(Vector3Int position) where T : TileBase
	{
		List<TileBase> possibleTiles = GetTiles(position);
		//if (possibleTiles == null)
		//	return null;
		List<T> returnTiles = new List<T>();
		if(possibleTiles != null)
			foreach (var tile in possibleTiles)
			{
				if (tile is T)
					returnTiles.Add((T)tile);
			}

		return returnTiles.Count != 0 ? returnTiles : null;
    }

	// returns all tiles with given type
	public List<T> GetTiles<T>() where T : TileBase
	{
		var keyList = positionByTiles[typeof(T)].Keys.ToList();
		List<T> returnTiles = keyList as List<T>;

		return returnTiles.Count != 0 ? returnTiles : null;
	}

	// returns all tiles with same type in given position
	public List<TileBase> GetTiles(Vector3Int position)
	{
		List<TileBase> returnTiles = new List<TileBase>();
		foreach (var entry in tilesByPosition)
		{
			var tileDict = entry.Value;
			if (tileDict.ContainsKey(position) && tileDict[position].Count > 0)
				returnTiles.AddRange(tileDict[position]);
		}

		return returnTiles.Count != 0 ? returnTiles : null;
	}

	public List<TileBase> GetTilesByRow<T>(int row) where T : TileBase
	{
		List<TileBase> returnList = new List<TileBase>();
		foreach (var typeDict in tilesByPosition)
		{
			if(typeDict.Key is T)
			{
				foreach (var entry in typeDict.Value)
				{
					if (entry.Key.y == row)
						returnList.AddRange(entry.Value);
				}
			}
		}
			
		return returnList;
	}

	public List<TileBase> GetTilesByColumn<T>(int column) where T : TileBase
	{
		List<TileBase> returnList = new List<TileBase>();
		foreach (var typeDict in tilesByPosition)
		{
			if (typeDict.Key is T)
			{
				foreach (var entry in typeDict.Value)
				{
					if (entry.Key.x == column)
						returnList.AddRange(entry.Value);
				}
			}
		}
		return returnList;
	}


	public Vector3Int? GetTilePosition(TileBase tile)
	{
		foreach (var entry in positionByTiles)
		{
			if (entry.Value.ContainsKey(tile))
				return entry.Value[tile];
		}

		return null;
	}

	public void AgitateCellContent(Vector3Int pos, TileBase culprit = null)
	{
		foreach(var tile in GetTiles(pos))
		{
			tile.CellContentChanged(culprit);
		}
	}

	public float GetDistanceBetween(Vector3Int start, Vector3Int end, Axis axis, Direction direction)
	{
		float distance = 0;
		if (axis == Axis.None || direction == Direction.None)
			return -1;
		// used when teleported
		Direction refDir = direction;
		Axis refAxis = axis;
		TileBase startingTile = GetTile(start);
		if (startingTile == null)
			return -1;
		TileBase tTile = startingTile.GetAdjacentTile<TileBase>(ref axis, ref refDir, true);
		while (!(tTile == null || tTile == startingTile))
		{
			Vector3Int tilePos = GetTilePosition(tTile).Value;

			if(tTile is ITeleporter)
			{
				switch(axis)
				{
					case Axis.X:
						distance += tilePos.x - start.x;
						break;
					case Axis.Y:
						distance += tilePos.y - start.y;
						break;
				}


				ITeleporter teleporter = tTile.GetComponent<ITeleporter>();
				start = GetTilePosition((TileBase)teleporter.ConnectedTeleporter).Value;
			}
					
			if (tilePos == end)
			{
				switch (axis)
				{
					case Axis.X:
						distance += tilePos.x - start.x;
						break;
					case Axis.Y:
						distance += tilePos.y - start.y;
						break;
				}
			}
					
			tTile = tTile.GetAdjacentTile<TileBase>(ref axis, ref refDir, true);
		}

		return distance;
	}
	public List<TileBase> GetTilesChain(TileBase startingTile, Axis axis, Direction direction)
	{
		if (axis == Axis.None || direction == Direction.None)
			Debug.LogError("Incorrect usage of GetTilesChain()");
		// used when teleported
		Direction refDir = direction;
		Axis refAxis = axis;

		List<TileBase> chain = new List<TileBase>
		{
			startingTile
		};


		TileBase tTile = startingTile.GetAdjacentTile<TileBase>(ref axis, ref refDir);
		while (!(tTile == null || tTile == startingTile))
		{
			if (tTile.CanMove() == MovementError.None)
			{
				chain.Add(tTile);
			}
			tTile = tTile.GetAdjacentTile<TileBase>(ref axis, ref refDir);
		}

		return chain;
	}

	public List<Tile_Draggable> GetTilesChain(Tile_Draggable startingTile, Axis axis, Direction direction, TileDrag tileDrag = null)
	{
		if (axis == Axis.None || direction == Direction.None)
			Debug.LogError("Incorrect usage of GetTilesChain()");
		// used when teleported
		Direction refDir = direction;
		Axis refAxis = axis;

		List<Tile_Draggable> chain = new List<Tile_Draggable>();
		if (tileDrag != null)
			startingTile.SetDragRules(tileDrag, refAxis, (refDir != direction ? true : false));
		chain.Add(startingTile);


		Tile_Draggable tTile = startingTile.GetAdjacentTile<Tile_Draggable>(ref axis, ref refDir);
		while (!(tTile == null || tTile == startingTile))
		{
			MovementError mError = tTile.CanMove();
			if (mError == MovementError.None)
			{
				if (tileDrag != null)
					tTile.SetDragRules(tileDrag, refAxis, (refDir != direction ? true : false));
				chain.Add(tTile);
			}

			else
			{
				if (mError.HasFlag(MovementError.StopsOthers))
					return new List<Tile_Draggable>();

			}

			tTile = tTile.GetAdjacentTile<Tile_Draggable>(ref axis, ref refDir);
		}

		return chain;
	}

#if UNITY_EDITOR // conditional compilation is not mandatory
	[CustomEditor(typeof(GridControl), true), CanEditMultipleObjects]
	public class GridControlEditor : Editor
	{

		// OnInspector GUI
		public override void OnInspectorGUI()
		{

			DrawDefaultInspector();
			GridControl targetScript = (GridControl)target;
			if (GUILayout.Button("ConnectTiles"))
			{
				targetScript.Validate();
			}
			if (GUILayout.Button("GenerateDynamicTextures"))
			{
				targetScript.Validate();
				foreach (var pos in targetScript.positionByTiles)
				{
					if (pos.Key is IDynamicSprite)
					{
						IDynamicSprite i = pos.Key as IDynamicSprite;
						i.GenerateDynamicSprite(true);
					}
				}
				Debug.Log("Generation complete.");
			}
			if (GUILayout.Button("ClearData"))
			{
				targetScript.tilesByPosition = new Dictionary<Type, Dictionary<Vector3Int, List<TileBase>>>();
				targetScript.positionByTiles = new Dictionary<Type, Dictionary<TileBase, Vector3Int>>();
			}

			if (GUILayout.Button("Show Tile Pos"))
			{
				Debug.Log(targetScript.GetTilePosition(targetScript.debugTile));
			}

		}
	}
#endif
}
