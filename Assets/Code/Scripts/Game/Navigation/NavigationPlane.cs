﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.ComponentModel;

/*
 * NavigationPlane builds around static objects
 * 
 * 
 * 
 */

public class NavigationPlane : MonoBehaviour
{
	public enum testShape
	{
		Circle,
		Rectangle
	};

	public int layer = 0;
	[Range(2, 100000)]
	public int xAxisNodes = 100;
	[Range(2, 100000)]
	public int yAxisNodes = 100;
	public testShape testObject = testShape.Circle;
	public float objectRadius = 1f;

	private IDictionary<Vector3, bool> navigationGrid = new Dictionary<Vector3, bool>();

	public void BuildPlane()

	{
		RectTransform bounds = GetComponent<RectTransform>();

		float xStep = bounds.sizeDelta.x / (xAxisNodes-1);
		float yStep = bounds.sizeDelta.y / (yAxisNodes-1);

		Vector3 startPos = bounds.anchoredPosition - bounds.sizeDelta/2;

		if (xStep < objectRadius*2 || yStep < objectRadius * 2)
		{
			Debug.LogError("Distance between objects are less than object diametre. Aborting...");
			return;
		}
		navigationGrid.Clear();

		for (float x = 0; x < xAxisNodes; x++)
		{
			for (float y = 0; y < yAxisNodes; y++)
			{
				GameObject testDummy;
				testDummy = new GameObject("CollisionDummy");
				
				Vector3 position = new Vector3(startPos.x + x * xStep, startPos.y + y * yStep, 0);
				testDummy.transform.position = position;
				switch (testObject)
				{
					case testShape.Circle:
						CircleCollider2D S = testDummy.AddComponent<CircleCollider2D>();
						S.radius = objectRadius;

						break;
					case testShape.Rectangle:
						BoxCollider2D B = testDummy.AddComponent<BoxCollider2D>();
						B.size.Set(objectRadius, objectRadius);
						break;
					default:
						break;
				}

				Collider2D testCollider = testDummy.GetComponent<Collider2D>();
				List<GameObject> returnList = new List<GameObject>(gameObject.FindGameObjectsWithLayer(layer));
				returnList.Remove(testDummy);
				
				foreach (var O in returnList)
				{
					Collider2D objCollider = O.GetComponent<Collider2D>();
					if (objCollider && testCollider.bounds.Intersects(objCollider.bounds))
					{
						navigationGrid[position] = false;
						Debug.Log(O.name);
						break;
					}
					else
						navigationGrid[position] = true;
				}
				DestroyImmediate(testDummy);






			}
		}
		
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	void OnDrawGizmosSelected()
	{
		if(navigationGrid.Count > 0)
			foreach(var V in navigationGrid)
			{
				if(V.Value)
					Gizmos.color = Color.green;
				else
					Gizmos.color = Color.red;
				Gizmos.DrawSphere(V.Key, objectRadius);
			}
	}
}

[CustomEditor(typeof(NavigationPlane))]
public class NavigationPlaneEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		NavigationPlane targetScript = (NavigationPlane)target;
		
		if (GUILayout.Button("Build Plane"))
		{
			targetScript.BuildPlane();
		}
	}
}
