﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
	private TargetingManager targetingManager;
	public GameObject parent;
	private bool deleteOnChange = true;
    
	public void Init(TargetingManager manager, bool _deleteOnChange = true)
	{
		targetingManager = manager;
		parent = targetingManager.gameObject;
		deleteOnChange = _deleteOnChange;

	}
	public void Init(GameObject _parent)
	{
		parent = _parent;
	}

	void OnDestroy()
	{
		if (targetingManager)
			targetingManager.RemoveTarget(gameObject);
	}

	// Start is called before the first frame update
	void Start()
	{
		if(!targetingManager && !parent)
		{
			Debug.LogError("Neiter TargetingManager nor parent object was assigned to target.");
			return;
		}
			
	}

	// Update is called once per frame
	void Update()
    {
		if (targetingManager && deleteOnChange && targetingManager.GetTarget() != gameObject)
			targetingManager.RemoveTarget(gameObject);
		else if (!parent)
			Destroy(gameObject);
	}

	void OnDrawGizmosSelected()
	{
		DebugHelper.DrawGizmoCross(Color.green,transform.position);
	}
}
