﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TargetingManager : MonoBehaviour
{

    protected List<GameObject> targets = new List<GameObject>();
    protected GameObject currentTarget;

	// Will create new target object on position and return it
    public virtual GameObject NewTarget(Vector3 position, bool deleteOnChange = true)
	{
		var nTarget = new GameObject("Target of " + gameObject.name);
		nTarget.transform.position = position;
		nTarget.AddComponent<Target>();

		return NewTarget(nTarget, deleteOnChange);
	}

	// not complete
	public GameObject NewTarget(GameObject targetObject, bool deleteOnChange = true)
	{
		currentTarget = targetObject;
		Target targetScript = currentTarget.GetComponent<Target>();

		if (targetScript)
			targetScript.Init(this, deleteOnChange);
		targets.Add(currentTarget);

		return currentTarget;
	}

	public GameObject GetTarget()
    {
        return currentTarget;
    }

    public Vector3 GetTargetPosition()
    {
        var _target = GetTarget();
        if (_target)
            return _target.transform.position;
        return default;
    }

    public void RemoveTarget(GameObject target)
    {
        targets.Remove(target);
		if (currentTarget == target)
			if (targets.Count > 0)
				currentTarget = targets.Last();
			else
				currentTarget = null;
		Destroy(target);
    }
	public void RemoveTarget()
	{
		targets.Remove(currentTarget);
		Destroy(currentTarget);
	}

	

}
