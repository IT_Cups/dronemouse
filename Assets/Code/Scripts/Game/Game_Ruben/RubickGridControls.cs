﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubickGridControls : GridControl, IVictoryController
{
	private GameObject pressDownTarget;

	// dragging controls data
	private Dictionary<string, TileDrag> movingTiles = new Dictionary<string, TileDrag>();
	public List<ICondition> VictoryConditions { get; set; } = new List<ICondition>();

	bool focus = true;

	// Start is called before the first frame update
	protected override void Start()
	{
		base.Start();
		
	}	

	// Update is called once per frame
	void Update()
	{
		if(focus)
		{
			if (Input.GetButtonDown("Click") && !pressDownTarget && !movingTiles.ContainsKey("Click"))
			{
				var mousePos = GetMousePosition();
				Vector3Int cellPos = (Vector3Int)grid.WorldToCell(mousePos);


				Tile_Draggable grabbedTile = GetTile<Tile_Draggable>(cellPos);

				if (grabbedTile && grabbedTile.CanMove() == MovementError.None)
				{
					TileDrag newDrag = gameObject.AddComponent<TileDrag>();
					newDrag.Init(grabbedTile, TileDrag.MovementType.Linear, 6);
					movingTiles.Add("Click", newDrag);
					//newDrag.SetPosition(grabbedTile.transform.position + new Vector3(-1000,0));
				}

				pressDownTarget = gameObject.CreateNewTarget(mousePos);
			}
			else if (Input.GetButtonUp("Click"))
			{
				if (movingTiles.ContainsKey("Click"))
				{
					var mousePos = GetMousePosition();
					TileDrag clickDrag = movingTiles["Click"];
					clickDrag.SetPosition(mousePos, true);
					clickDrag.FinalizeMovement();

					movingTiles.Remove("Click");
				}
				Destroy(pressDownTarget);
			}
			else if (pressDownTarget)
			{
				var mousePos = GetMousePosition();
				// update grid target's position
				pressDownTarget.transform.position = mousePos;

				if (movingTiles.ContainsKey("Click"))
				{
					TileDrag clickDrag = movingTiles["Click"];

					clickDrag.SetPosition(mousePos);

				}
			}
		}
		
	}

	public override Vector3Int? ConnectTile(TileBase tile, bool stealthy = false)
	{
		var returnValue = base.ConnectTile(tile, stealthy);
		if (returnValue.HasValue && tile is ICondition)
			VictoryConditions.Add(tile as ICondition);
		return returnValue;
	}


	private Vector3 GetMousePosition()
	{
		Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mousePos.z = 0;
		return mousePos;
	}

	public void VictoryActions()
	{
		IUIElement victoryUI = UIController.Elements["victory"];
		focus = false;
		victoryUI?.Show();
	}

	public void CheckVictoryConditions()
	{
		bool victory = true;
		foreach(var condition in VictoryConditions)
		{
			if (!condition.IsMet())
			{
				victory = false;
				break;
			}	
		}
		if (victory)
			VictoryActions();
	}
}
