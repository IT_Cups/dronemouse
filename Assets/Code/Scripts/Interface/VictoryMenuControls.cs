﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryMenuControls : MonoBehaviour, IUIElement
{
    private Canvas canvas;

    public bool IsHidden => !canvas.enabled;

    public string id => "victory";

    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas>();
        Hide();
        canvas.worldCamera = Camera.main;
    }

    public void Show()
    {
        canvas.enabled = true;
    }
    public void Hide()
    {
        canvas.enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
