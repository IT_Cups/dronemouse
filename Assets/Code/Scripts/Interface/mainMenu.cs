﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainMenu : MonoBehaviour
{
    public GameObject VictoryUI;
    public void playGame()
    {
        GameObject UIVictoryObject = Instantiate(VictoryUI);
        IUIElement elem = UIVictoryObject.GetComponent<IUIElement>();
        UIController.AddElement(elem.id, elem);
        DontDestroyOnLoad(UIVictoryObject);
        SceneManager.LoadScene(1);
    }
 

    public void quitGame()
    {
        Application.Quit();
    }
}
