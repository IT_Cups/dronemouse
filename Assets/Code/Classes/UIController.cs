﻿using UnityEngine;
using System.Collections.Generic;

public static class UIController
{
	public static Dictionary<string, IUIElement> Elements = new Dictionary<string, IUIElement>();

	public static void AddElement(string id, IUIElement element)
	{
		if (Elements.ContainsKey(id))
			Debug.LogError("UIController already contains element with id " + id);
		else
			Elements.Add(id, element);
	}
	public static void RemoveElement(IUIElement element)
	{
		foreach(var entry in Elements)
		{
			if (entry.Value == element)
			{
				Elements.Remove(entry.Key);
				break;
			}

		}
	}
	public static void RemoveElement(string id)
	{
		Elements.Remove(id);
	}
}
